package main

import "github.com/gocolly/colly"
import "github.com/PuerkitoBio/goquery"
import "encoding/json"
import "os"

type Service struct {
    Name string
    URL string
}

type Network struct {
    Name string
    Services []Service
}

type Group struct {
    Name string
    Networks []Network
}

var groups []Group

func main() {
    scrapeBBC()
}



func scrapeBBC() {
    c := colly.NewCollector(
        colly.AllowedDomains("www.bbc.co.uk"),
    )

    // A group groups TV, Radio, or similar programmes together
    c.OnHTML("div.sch-group", scrapeBBC_Group)

    c.Visit("https://www.bbc.co.uk/schedules")

    enc := json.NewEncoder(os.Stdout)
    enc.Encode(groups)
}

func scrapeBBC_Group(e *colly.HTMLElement) {
    group := Group {
        Name: e.ChildText("h2.alpha"),
    }
//    fmt.Println("Group: " + group.Name)
    groups = append(groups, group)

    e.ForEach("div.sch-network-name", scrapeBBC_Network)
}

func scrapeBBC_Network(i int, e *colly.HTMLElement) {
    network := Network {
        Name: e.Text,
    }
//    fmt.Println("\tNetwork: " + network.Name)
    group := &groups[len(groups)-1]
    (*group).Networks = append((*group).Networks, network)

    // Unfortunately Networks & Services are mixed in this list, that's why
    // we need to find all Networks and then jump along to the siblings from
    // there til we reach the next Network.
    //
    // Worse, though, if a Network contains just one Service, it isn't listed
    // as a Network in the main list, but as a Service, with a Network child:
    //
    // * Network A
    //    - Service A:A
    //    - Service B:A
    // * Network B
    //    - Service B:A
    //    - Service B:B
    // * Service C:_
    //    - Network C
    //
    // I have no idea who came up with this "structure"...

    //TODO: Figure out why
    //     e.DOM.Parent().Is("div.sch-service-row deemphasize")
    // doesn't work...
    class, _ := e.DOM.Parent().Attr("class")
    if class == "sch-service-row deemphasize" {
        /* the Network we found is a child of the only Service it provides */
        scrapeBBC_Service(0, e.DOM.Parent())
    } else {
        e.DOM.NextUntil("div.sch-network-name").Each(scrapeBBC_Service)
    }
}

func scrapeBBC_Service(i int, s *goquery.Selection) {
    if i != 0 && s.Children().First().Is("div.sch-network-name") {
        // We are looking for another Service to add, but we've found
        // a collapsed single-Service Network instead.
        return
    }
    url, exists := s.Attr("href")
    if !exists {
        panic("service doesn't contain link")
    }
    name := s.ChildrenFiltered("div.sch-service-name").Text()
    if name == "" {
        // If we are a Network providing a single Service, use the
        // Network's Name as the Service
        name = s.ChildrenFiltered("div.sch-network-name").Text()
    }
//    fmt.Println("\t\tService: " + name + " at: " + url)

    service := Service {
        Name: name,
        URL: url,
    }
    networks := groups[len(groups)-1].Networks
    network := &networks[len(networks)-1]
    (*network).Services = append((*network).Services, service)
}

